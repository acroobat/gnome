# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gdm option-renames [ renames=[ 'xdmcp remote-login' ] ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]
require udev-rules

PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection
    journald
    plymouth [[ description = [ enable display switching from plymouth ] ]]
    remote-login [[ description = [ add support for remote logins ] ]]
    tcpd [[ description = [ use tcp-wrappers to secure xdcmp requests ] requires = [ remote-login ] ]]
    wayland
    ( linguas: af am an ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs csb cy da
               de dz el en_CA en_GB en@shaw eo es et eu fa fi fr fur fy ga gl gu gv he hi hr hu hy
               id is it ja ka kk km kn ko ku ky lt lv mai mg mi mk ml mn mr ms nb nds ne nl nn nso
               oc or pa pl ps pt pt_BR ro ru rw si sk sl sq sr sr@latin sv ta te tg th tr ug uk uz
               uz@cyrillic vi wa xh zh_CN zh_HK zh_TW zu )
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.8.1]
        dev-libs/libxml2:2.0 [[ note = [ required for xmllint ] ]]
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.20]
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.44.0]
        media-libs/libcanberra[>=0.4][providers:gtk3]
        media-libs/fontconfig[>=2.5]
        sys-apps/accountsservice[>=0.6.35][gobject-introspection][providers:elogind?][providers:systemd?]
        sys-apps/keyutils
        sys-libs/pam
        (
            x11-libs/libX11
            x11-libs/libXau
            x11-libs/libxcb
            x11-libs/libXext
            x11-libs/libXrandr
            x11-libs/libXft
            x11-libs/libXi
            x11-libs/libXinerama
        ) [[ note = [ X dependencies ] ]]
        x11-libs/gtk+:3[>=2.91.1]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.12] )
        journald? ( sys-apps/systemd )
        providers:elogind? ( sys-auth/elogind )
        providers:systemd? ( sys-apps/systemd )
        plymouth? ( sys-boot/plymouth )
        remote-login? ( x11-libs/libXdmcp )
        tcpd? ( sys-apps/tcp-wrappers )
    run:
        gnome-desktop/gnome-session
        gnome-desktop/gnome-settings-daemon
        x11-apps/xhost [[ note = [ gdm-x-session uses this ] ]]
        x11-server/xorg-server[>=1.12] [[ note = [ for multi-seat ] ]]
    test:
        dev-libs/check[>=0.9.4]
    recommendation:
        gnome-desktop/gnome-shell
        gnome-desktop/gnome-keyring[pam] [[
            note = [ provides pam_gnome_keyring ]
            description = [ Provides keyring integration for GNOME sessions ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/gdm-support-elogind.patch
)

src_prepare() {
    # The below patch breaks gdm's pam configuration with logind, so
    # we apply it conditionally
    option providers:elogind && expatch "${FILES}"/gdm-pam-enable-elogind.patch

    autotools_src_prepare
    echo "WantedBy=graphical.target" >> data/gdm.service.in
}

src_configure() {
    econf "--localstatedir=/var"                                \
          "--with-xinerama"                                     \
          "--without-selinux"                                   \
          $(option_with remote-login xdmcp)                     \
          $(option_with tcpd tcp-wrappers)                      \
          $(option_with plymouth)                               \
          $(option_enable wayland wayland-support)              \
          $(option_enable gobject-introspection introspection)  \
          $(option_enable journald systemd-journal)             \
          "--with-default-pam-config=exherbo"                   \
          "--with-lang-file=/etc/locale.conf"                   \
          "--with-dmconfdir=/etc/X11/dm"                        \
          "--with-initial-vt=1"                                 \
          "--enable-ipv6"                                       \
          "--enable-gdm-xsession"                               \
          "--enable-user-display-server"                        \
          "--without-libaudit"                                  \
          "--with-run-dir=/run/gdm/"                            \
          "--with-screenshot-dir=/run/gdm/"                     \
          "--with-udevdir=$UDEVDIR"                             \
          "--with-xauth-dir=/run/gdm/"
}

