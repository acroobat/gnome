# Copyright 2019 Calvin Walton <calvin.walton@kepstin.ca>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require gsettings
require gtk-icon-cache
require freedesktop-desktop
require vala [ with_opt=true vala_dep=true ]
require python [ blacklist=2 multibuild=false ]
require meson


SUMMARY="Editor for the GNOME environment"
DESCRIPTION="
While aiming at simplicity and ease of use, gedit is a powerful general purpose text editor which
features full support for UTF-8, configurable highlighting for various languages and many other
features making it a great editor for advanced tasks.
"
LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk-doc"

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.18]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libpeas:1.0[>=1.14.1][python_abis:*(-)?]
        dev-libs/libxml2:2.0[>=2.5.0]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        gnome-desktop/gobject-introspection:1[>=1.42.0]
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/gspell:1[>=0.2.5]
        gnome-desktop/gtksourceview:4.0[>=4.0.2][gobject-introspection]
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection]
    suggestion:
        gnome-desktop/gedit-plugins [[ description = [ Plugins for additional features ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dintrospection=true
    -Dplugins=true
    -Denable-gvfs-metadata=yes
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'gtk-doc documentation' vapi )

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

