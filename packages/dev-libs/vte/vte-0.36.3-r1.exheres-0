# Copyright (C) 2008 Stephen Bennett <spb@exherbo.org>
# Copyright (C) 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="A terminal emulator widget"
HOMEPAGE="http://developer.gnome.org/arch/gnome/widgets/vte.html"

LICENCES="GPL-2"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc
    ( linguas: am ang ar as ast az be be@latin bg bn bn_IN bs ca ca@valencia cs cy da de dz el en_CA
               en_GB en@shaw eo es et eu fa fi fr fur ga gl gu he hi hr hu id is it ja ka kn ko ku ky
               li lt lv mai mi mk ml mn mr ms nb nds ne nl nn oc or pa pl pt pt_BR ro ru rw si sk sl
               sq sr sr@latin sv ta te th tr ug uk uz@cyrillic vi wa xh zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        virtual/pkg-config[>=0.19]
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        x11-libs/gtk+:3[>=3.1.9][gobject-introspection?]
        dev-libs/glib:2[>=2.31.13]
        x11-libs/pango[>=1.22.0][gobject-introspection?]
        sys-libs/ncurses
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.0] )
        !dev-libs/vte:0 [[
            description = [ Installs the same file ]
            resolution = uninstall-blocked-after
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=( '--disable-deprecation' CFLAGS="${CFLAGS} -D_GNU_SOURCE" )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gtk-doc' 'gobject-introspection introspection' )

src_prepare() {
    default
    # Respect datarootdir
    edo sed -e 's:itlocaledir = $(prefix)/$(DATADIRNAME)/locale:itlocaledir = $(datarootdir)/locale:' \
        -i "${WORK}"/po/Makefile.in.in
}

